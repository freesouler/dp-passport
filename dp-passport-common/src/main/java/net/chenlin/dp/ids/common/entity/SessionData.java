package net.chenlin.dp.ids.common.entity;

import java.io.Serializable;

/**
 * session用户数据
 * @author zcl<yczclcn@163.com>
 */
public class SessionData implements Serializable {

    private static final long serialVersionUID = 758614707396802783L;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 用户登录名
     */
    private String userAlias;

    /**
     * 是否登录：1：登录状态，0：未登录
     */
    private Integer isLogin;

    /**
     * 用户可用状态：1：正常，0：禁用
     */
    private Integer userEnable;

    /**
     * 是否记住我
     */
    private Boolean rememberMe;

    /**
     * 登录类型：1：web端，2：app端
     */
    private Integer loginType;

    /**
     * constructor
     * @param userId
     * @param userAlias
     * @param userEnable
     */
    public SessionData(Long userId, String userAlias, Integer userEnable, Integer loginType) {
        this.userId = userId;
        this.userAlias = userAlias;
        this.userEnable = userEnable;
        this.loginType = loginType;
    }

    /**
     * constructor
     */
    public SessionData() { }

    /**
     * getter for userId
     * @return
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * setter for userId
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * getter for userAlias
     * @return
     */
    public String getUserAlias() {
        return userAlias;
    }

    /**
     * setter for userAlias
     * @param userAlias
     */
    public void setUserAlias(String userAlias) {
        this.userAlias = userAlias;
    }

    /**
     * getter for userEnable
     * @return
     */
    public Integer getUserEnable() {
        return userEnable;
    }

    /**
     * setter for userEnable
     * @param userEnable
     */
    public void setUserEnable(Integer userEnable) {
        this.userEnable = userEnable;
    }

    /**
     * getter for isLogin
     * @return
     */
    public Integer getIsLogin() {
        return isLogin;
    }

    /**
     * setter for isLogin
     * @param isLogin
     */
    public void setIsLogin(Integer isLogin) {
        this.isLogin = isLogin;
    }

    /**
     * getter for rememberMe
     * @return
     */
    public Boolean getRememberMe() {
        return rememberMe;
    }

    /**
     * setter for rememberMe
     * @param rememberMe
     */
    public void setRememberMe(Boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    /**
     * getter for loginType
     * @return
     */
    public Integer getLoginType() {
        return loginType;
    }

    /**
     * setter for loginType
     * @param loginType
     */
    public void setLoginType(Integer loginType) {
        this.loginType = loginType;
    }

    /**
     * to string
     * @return
     */
    @Override
    public String toString() {
        return "SessionData{" +
                "userId=" + userId +
                ", userAlias='" + userAlias + '\'' +
                ", isLogin=" + isLogin +
                ", userEnable=" + userEnable +
                ", rememberMe=" + rememberMe +
                ", loginType=" + loginType +
                '}';
    }

}
